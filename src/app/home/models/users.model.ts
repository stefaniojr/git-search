import { Expose } from 'class-transformer';

export class Users {
    @Expose({ name: 'total_count' })
    public totalCount: number;

    @Expose({ name: 'incomplete_results' })
    public incompleteResults: boolean;

    @Expose({ name: 'items' })
    public items: ItemUser[];
}

export class ItemUser {
    @Expose({ name: 'login' })
    public login: string;

    @Expose({ name: 'id' })
    public id: number;

    @Expose({ name: 'node_id' })
    public nodeId: string;

    @Expose({ name: 'avatar_url' })
    public avatarUrl: string;

    @Expose({ name: 'gravatar_id' })
    public gravatarId: string;

    @Expose({ name: 'url' })
    public url: string;

    @Expose({ name: 'html_url' })
    public htmlUrl: string;

    @Expose({ name: 'followers_url' })
    public followersUrl: string;

    @Expose({ name: 'following_url' })
    public followingUrl: string;

    @Expose({ name: 'gists_url' })
    public gistsUrl: string;

    @Expose({ name: 'starred_url' })
    public starredUrl: string;

    @Expose({ name: 'subscriptions_url' })
    public subscriptionsUrl: string;

    @Expose({ name: 'organizations_url' })
    public organizationsUrl: string;

    @Expose({ name: 'repos_url' })
    public reposUrl: string;

    @Expose({ name: 'events_url' })
    public eventsUrl: string;

    @Expose({ name: 'received_events_url' })
    public receivedEventsUrl: string;

    @Expose({ name: 'type' })
    public type: string;

    @Expose({ name: 'site_admin' })
    public siteAdmin: string;

    @Expose({ name: 'score' })
    public score: number;
}
