import { Component, OnInit } from '@angular/core';
import { API } from '../services/api';
import { User } from './models/user.model';
import { ItemUser, Users } from './models/users.model';
import { MdbModalRef, MdbModalService } from 'mdb-angular-ui-kit/modal';
import { ModalErrorComponent } from './../components/modal-error/modal-error.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  modalRef: MdbModalRef<ModalErrorComponent>;

  results: Users;

  resultsTo: string;
  users: Array<ItemUser>;

  userData: User;

  public isLoading: boolean;

  constructor(private api: API, private modalService: MdbModalService) {
    this.resultsTo = "";
    this.isLoading = false;
  }

  ngOnInit(): void {
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**
 * Filtra a pesquisa
 */
  async filter(event: any) {
    this.isLoading = true;
    const filterValue = (event.target as HTMLInputElement).value.trim();
    await this.delay(3000);
    if (filterValue) {
      // bloco try-catch. chama o modal quando algo dá errado.
      try{
        this.resultsTo = filterValue;
        const response = await this.api.getUsers(filterValue);
        this.results = Object.assign(response);
        this.users = this.results.items;
      } catch(e){
        this.modalRef = this.modalService.open(ModalErrorComponent, {modalClass: 'modal-sm modal-dialog-centered'});
        this.resultsTo = "";
        this.results = undefined;
      }
      
    }
    if (!filterValue) {
      this.resultsTo = "";
      this.results = undefined;
    }

    this.isLoading = false;

  }
}
