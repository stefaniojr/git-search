import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CardComponent } from './card/card.component';
import { ModalComponent } from './modal/modal.component';
import { ModalErrorComponent } from './modal-error/modal-error.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    CardComponent,
    ModalComponent,
    ModalErrorComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    CardComponent,
    ModalComponent
  ]
})
export class ComponentsModule { }
