import { Component, Input, OnInit } from '@angular/core';
import { API } from 'src/app/services/api';
import { MdbModalRef, MdbModalService } from 'mdb-angular-ui-kit/modal';
import { ModalComponent } from '../modal/modal.component';
import { User } from 'src/app/home/models/user.model';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(1000, style({ opacity: 1 })),
      ]),
      transition('* => void', [animate(1000, style({ opacity: 0 }))]),
    ]),
  ],
})
export class CardComponent implements OnInit {
  modalRef: MdbModalRef<ModalComponent>;

  @Input()
  avatar: string;

  @Input()
  login: string;

  @Input()
  github: string;

  @Input()
  score: number;

  user: User;


  constructor(private api: API, private modalService: MdbModalService) {

  }

  async ngOnInit() {
  }

  async openModal() {
    const response = await this.api.getUser(this.login);
    this.user = Object.assign(response);
    this.modalRef = this.modalService.open(ModalComponent, {
      modalClass: 'modal-lg modal-dialog-centered', data: { user: this.user }
    });

  }
}
