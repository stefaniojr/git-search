import { Component, OnInit } from '@angular/core';
import { MdbModalRef } from 'mdb-angular-ui-kit/modal';
import { User } from 'src/app/home/models/user.model';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  user: User;

  avatar: string;
  name: string;
  login: string
  following: number;
  date: Date;
  followers: number;
  url: string;

  constructor(public modalRef: MdbModalRef<ModalComponent>) { }

  async ngOnInit() {
    this.avatar = this.user['avatar_url'];
    this.name = this.user['name'];
    this.login = this.user['login'];
    this.following = Number(this.user['following']);
    this.date = this.user['created_at'];
    this.followers = Number(this.user['followers']);
    this.url = this.user['html_url'];
  }
}
