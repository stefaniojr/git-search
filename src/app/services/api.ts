import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Users } from '../home/models/users.model';
import { User } from '../home/models/user.model';

@Injectable({
    providedIn: 'root',
})
export class API {
    constructor(public http: HttpClient) { }

    /**
     * URLS
     */

    public static API_BASE_USERS_URL = 'https://api.github.com/search/users?q=';
    public static API_BASE_USER_URL = 'https://api.github.com/users/';

    /**
     * Recupera os usuários com base no termo pesquisado
     */
    public async getUsers(word: string): Promise<Users> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get<Users>(API.API_BASE_USERS_URL + word, httpOptions).toPromise();
    }

    /**
     * Recupera um usuário específico
     */
    public async getUser(word: string): Promise<User> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get<User>(API.API_BASE_USER_URL + word, httpOptions).toPromise();
    }
}
